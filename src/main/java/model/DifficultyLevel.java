
package model;

/**
 * This enumeration represents the difficulty of the minigames.
 *
 */
public enum DifficultyLevel {

    /**
     * Represent the easy level.
     */
    EASY,

    /**
     * Represent the normal level.
     */
    NORMAL,

    /**
     * Represent the hard level.
     */
    HARD;
}
